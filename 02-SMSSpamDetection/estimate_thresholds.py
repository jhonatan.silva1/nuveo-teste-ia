import pickle
from sklearn.metrics import roc_curve

# O calculo dos limiares será feito sobre o conjunto de validação, pois é um conjunto nao utilizado durante o treinamento para não ocorrer enviesamento

val_set = open("TrainingSet/sms-hamspam-val.csv", "r")

target_ham = []
target_spam = []
sms = []

# Preenchendo as listas de valores esperados para ham e spam
for row in val_set:
    r = row.split()
    target_ham.append(0 if r[0] == "spam" else 1)
    target_spam.append(1 if r[0] == "spam" else 0)

    sms.append(" ".join(r[1:]))

# Carregando o modelo
sms_model = pickle.load(open("Model/sms_model_v1.pkl", "rb"))
# Realizando as predições para todos os SMS do conjunto de validação
preds = sms_model.predict_proba(sms)

# Calculando a ROC para obter todos valores de fpr e o limiar associado para Ham e Spam
fpr_ham, tpr_ham, thresholds_ham = roc_curve(target_ham, preds[:,0])
fpr_spam, tpr_spam, thresholds_spam = roc_curve(target_spam, preds[:,1])

no_spam_as_ham_thresh = 0
no_ham_as_spam_thresh = 0

# Estimativa do limiar de classificação de ham onde nao existe spam classificado como ham para o conjunto de validação
# Basicamente, a partir do momento que a taxa de fpr é maior que 0, significa que temos algum spam classificado como ham, entao é retornado o limiar anterior
for fp in range(len(fpr_ham)):
    if fpr_ham[fp] > 0:
        no_spam_as_ham_thresh = thresholds_ham[fp-1]
        break
# Estimativa do limiar de classificação de spam onde nao existe ham classificado como spam para o conjunto de validação, a estratégia é a mesma descrita acima
for fp in range(len(fpr_spam)):
    if fpr_spam[fp] > 0:
        no_ham_as_spam_thresh = thresholds_spam[fp-1]
        break

print("Threshold for not classifying spam as ham: ", no_spam_as_ham_thresh)
print("Threshold for not classifying ham as spam: ", no_ham_as_spam_thresh)




