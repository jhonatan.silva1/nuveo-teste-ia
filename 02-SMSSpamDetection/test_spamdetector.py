import unittest
import spamdetector
from unittest.mock import Mock

class TestSpamDetector(unittest.TestCase):
    
    def test_basics(self):
        """ Testing basic function calls
        """
        sd = spamdetector.SpamDetector()
        test_str = 'K:)k..its good:)when are you going?'
        self.assertIsNotNone(sd.prob_spam(test_str))
        self.assertIsNotNone(sd.is_spam(test_str))


    def test_with_dataset(self):
        """ Testing with all sms from the validation dataset
        """
        sd = spamdetector.SpamDetector()
        val_set = open("TrainingSet/sms-hamspam-val.csv", "r")
        sms = []
        for row in val_set:
            r = row.split()
            sms.append(" ".join(r[1:]))

        self.assertEqual(len(sms), len([sd.prob_spam(s) for s in sms]))
        self.assertEqual(len(sms), len([sd.is_spam(s) for s in sms]))
    
    def test_is_spam(self):
        """ Testing the operation modes from 'is_spam', asserting never classifying ham as spam when op=2 and
        never classifying spam as ham when op=1 (Thresholds calculated in 'estimate_thresholds.py')
        """
        sd = spamdetector.SpamDetector()
        val_set = open("TrainingSet/sms-hamspam-val.csv", "r")
        sms = []
        targets = []
        for row in val_set:
            r = row.split()
            targets.append(r[0])
            sms.append(" ".join(r[1:]))

        for s in range(len(sms)):
            if targets[s] == 'ham':
                self.assertFalse(sd.is_spam(sms[s], 2))
            if targets[s] == 'spam':
                self.assertTrue(sd.is_spam(sms[s], 1))

if __name__ == '__main__':
    unittest.main()