import pickle

class SpamDetector:
    """SMS Spam Detector"""

    def __init__(self):
        self.sms_model = pickle.load(open("Model/sms_model_v1.pkl", "rb"))
        # Limiares calculados no script estimate_thresholds.py
        self.no_spam_as_ham_thresh = 0.94
        self.no_ham_as_spam_thresh = 0.72

    def prob_spam(self, sms):
        """calculates the probability that 'sms' is spam

        :param token: (str)
        :return: (float) probability 'token' is spam based on training emails
        """
        val = self.sms_model.predict_proba([sms])[0]
        return val[1]

    def is_spam(self, sms, op=0):
        """checks if 'sms' is spam or not 

        :param sms: (str) message to be classified as spam
        :param op: (int) indicates operation mode, 1 for no spam as ham and 2 for no ham as spam, otherwise return True if spam probability is bigger than 0.5
        :return: (bool) either 'sms' is spam or not
        """
        val = self.sms_model.predict_proba([sms])[0]
                
        thresh = 0.5

        if op == 1:
            return not val[0] > self.no_spam_as_ham_thresh
        elif op == 2:
            return val[1] > self.no_ham_as_spam_thresh
        else:
            return val[1] > thresh
        


