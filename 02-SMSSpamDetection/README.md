# SMS Spam Detector

Class for detecting and predicting SMS probability of being spam

## Requirements

 - Python 3.7+
 - scikit-learn
 - unittest

## Running
 
### Using the class SpamDetector
Declare it as an object, call the functions `prob_spam` for calculating the probability that 'sms' is spam and `is_spam` for checking either 'sms' is spam or not, `is_spam` has two operation modes, 1 for no spam as ham and 2 for no ham as spam, otherwise return True if spam probability is bigger than 0.5 

### Estimate Thresholds
Simply call `python estimate_thresholds.py` this code will estimate thresholds for not classifying spam as ham and vice-versa

### Testing
Calling `python test_spamdetector.py` will rund all tests