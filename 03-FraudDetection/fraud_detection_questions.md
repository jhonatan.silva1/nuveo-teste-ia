# Instruções
- As questões devem ser respondidas neste próprio documento `markdown`.
- Quaisquer referências, como papers, páginas web, imagens, etc, podem ser utilizadas para composição das respostas. Porém, é recomendado que as respostas sejam objetivas e diretas.
- Não há uma resposta correta para cada pergunta, de forma que a avaliação não será feita com comparação da resposta fornecida com um gabarito. A ideia da proposta é fornecer a oportunidade de demonstrar o conhecimento na construção de uma solução para o cenário proposto, levantar possíveis limitações, impedimentos e como os problemas podem ser evitados ou minimizados, sem que seja necessário dedicar tempo com experimentos, treinos e determinação de parâmetros de forma empírica.


# Perguntas

### 1-Explique, de forma objetiva, qual abordagem seria escolhida para solução do problema proposto. Caso a abordagem envolva Redes Neurais, descreva qual seria a arquitetura de rede utilizada e por que.

R: Sabendo que é um problema mais complexo do que os frequentementes abordados na literatura, envolvendo além da verificação do autor, também a avaliação de assinaturas forjadas habilidosas, proponho não uma mas duas abordagens envolvendo redes neurais:
1) Verificação de manuscrito - A verificação do autor pode ser feita para, logo de antemão, identificar assinaturas forjadas, pois, em caso de resposta negativa do método, a assinatura questionada deve ser classificada como Simulated, em caso de positivo (assinaturas de referencia e questionada foram reconhecidas como de um mesmo autor), ainda existem as possibilidades de classificação em Disguised ou Genuine, neste ponto, a segunda abordagem decidirá a classificação. As técnicas para implementação da tarefa de verificação de manuscrito funcionam a partir da extração de caracteristicas locais, e uma solução que apresenta um resultado promissor é a "Attention based Writer Independent Verification" [1] que conta com uma arquitetura complexa, utilizando dois inputs e um bloco de extração de caracteristicas que reutiliza a arquitetura da VGG16 na forma de uma rede neural siamesa, conectando à um bloco de Multi-Head Cross Attention [2], que é uma técnica que utiliza multiplos mecanismos de Attention(uma técnica que realça a informação mais relevante do seu input) de forma cruzada, e encerra com blocos de camadas convolucionais, pooling, dropout, flatten, e a camada de output completamente conectada com saída 1x2, com função de ativação softmax que classifica com duas confianças o par de assinaturas como sendo de um mesmo autor ou não.

2) Verificação de assinatura - Esta solução vai distinguir assinaturas entre Disguised e Genuine. Ao contrario do tarefa anterior, o foco desta tarefa é extração e comparação de características globais, uma solução viável para esta tarefa é o treinamento de um Autoencoder, que é uma categoria de arquiteturas de redes neurais que tem a capacidade de reduzir drasticamente a dimensão de sua entrada, e reconstrui-la a partir da matriz em dimensão reduzida. Com o Autoencoder em devido funcionamento ao realizar a inferencia de uma imagem de assinatura, seria de interesse apenas a saída do bloco de encoding (a entrada em dimensão reduzida), pois seria facilmente comparado com outra matriz em dimensão reduzida originada do encoding de outra imagem de assinatura. Uma comparação de dois vetores de características pode ser implementada como uma simples distancia, podendo ser distancia de cosseno, euclidiana ou manhattan. O trabalho "Learning features for offline handwritten signature verification using deep convolutional neural networks" [3] utiliza uma abordagem bastante similar a abordagem descrita neste paragrafo.

No diagrama abaixo é possível conferir a organização da solução, sendo S1 uma assinatura do conjunto de referencia e S2 uma assinatura questionada
![Solution Diagram](solution_diagram.png)

Como os métodos propostos utilizam apenas duas assinaturas de entrada e temos um conjunto de assinaturas de referencia, proponho que seja feita a comparação da assinatura questionada com todas as assinaturas de referencia e tomar a decisão final como sendo a da maioria, minimizando casos de erro.


### 2-Liste e explique qual a linguagem, framework, pacotes e ferramentas seriam utilizados para construção da solução.

R: Ambas redes neurais propostas poderiam ser implementadas em python utilizando os frameworks Keras com background Tensorflow para construção, manipulação e treinamento das arquiteturas descritas; OpenCV e Numpy para manipulação da imagens e matrizes; Matplotlib para visualização de métricas, gráficos e curvas de perda; JupyterLab como interface de desenvolvimento; E CUDA caso houver a disponibilidade de uma GPU Nvidia para o treinamento.

### 3-Quais os principais parâmetros da solução e como melhor otimizar a escolha de tais parâmetros?

R: Existem os hiperparametros das redes neurais, relacionados ao algoritmo de treino, entre estes estão o Learning Rate, Quantidade de epocas e Batch size, estes parametros geralmente são escolhidos por tentativa e erro. Além dos hiperparametros do treino, existem os parametros fundamentais para decisão dos classificadores, os limiares de referencia, estes são comparados com a confiança/similaridade/distancia retornada pelas soluções para que a classe da assinatura questionada seja decidida, a melhor forma de otimizar a escolha dos parametros de limiar é através da estimativa de métricas em um conjunto de teste, como, por exemplo, taxa de falsa aceitação e taxa de falsa rejeição.

### 4-Quais limitações podem impactar a solução proposta, seja em relação à própria abordagem escolhida ou em relação ao dataset fornecido para treino e teste?

R: Por se tratarem de redes neurais, seu uso em CPU é custoso podendo ser lento, portanto, é preferencial que as soluções estejam funcionando em hardware com GPU. Em relação aos datasets de treino e teste fornecidos, é inviável o treinamento das soluções propostas com a quantidade de dados fornecida, pois um conjunto de treino que não tem representatividade o suficiente produz modelos tendenciosos e sobreajustados, além disso, o conjunto de teste também não é generalizavel o suficiente, produzindo métricas enviesadas. Para contornar estes problemas é necessária a confecção de mais dados e/ou utilização de modelos pré-treinados e datasets publicos.

### 5-Qual seria o prazo proposto para o desenvolvimento da solução proposta?

R: Considerando a posição de Engenheiro em tempo integral, suponho que seriam necessários em torno de 2 meses já dando margem para eventuais problemas, uma vez que já existem implementações open-source das abordagens propostas, mas deverão ocorrer ajustes de arquitetura, experimentos, estimativa de métricas, testes de cenários de uso, avaliação, confecção e validação dos dados.

### 6-Considerando um cenário real de uso do método desenvolvido para detecção de fraudes em assinatura, o erro mais prejudicial seria retornar que uma assunatura é legítima quando trata-se de uma assinatura forjada. Como minimizar este tipo de erro?

R: Esse tipo de erro é muito prejudicial e pode ser minimizado avaliando os limiares de decisão para ambos os métodos propostos, uma forma de fazer esta avaliação é gerando uma curva ROC, que é construída pelos valores de falsos positivos e verdadeiros positivos obtidos do uso do modelo no conjunto de testes em cada limiar de um intervalo de limiares, a partir da curva ROC pode ser escolhido um ponto de operação que minimiza os erros de falso positivo.

### 7-O que poderia ser alterado na disposição de dados para auxiliar o resultado da solução proposta?

R: Um dos problemas da disposição de dados fornecida é o desbalanceamento entre os conjuntos de assinaturas questionadas, podendo enviesar o modelo no momento de treinamento, outro problema já comentado, é a limitação da quantidade de imagens e autores, influenciando na representatividade e capacidade de generalização do modelo treinado. Portanto, para auxiliar no resultado da solução é ideal ter quantidades similares de imagens entre as classes de assinaturas questionadas e adição de mais autores.

## Referencias

 - [1] Shaikh, Mohammad Abuzar, et al. "Attention based writer independent verification." 2020 17th International Conference on Frontiers in Handwriting Recognition (ICFHR). IEEE, 2020.
 - [2] Vaswani, Ashish, et al. "Attention is all you need." Advances in neural information processing systems. 2017.
 - [3] Hafemann, Luiz G., Robert Sabourin, and Luiz S. Oliveira. "Learning features for offline handwritten signature verification using deep convolutional neural networks." Pattern Recognition 70 (2017): 163-176.